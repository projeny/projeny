"""
Tools for annotating sections in a file.  The sections can be included verbatim in other files.

Taken from https://github.com/paver/paver/blob/master/paver/doctools.py
"""

from pathlib import Path
import re

class BuildFailure(Exception):
    """Represents a problem with some part of the section parsing."""
    pass

def debug(m):
    print(m)

_sectionmarker = re.compile(r'\[\[\[section\s+(.+)\]\]\]')
_endmarker = re.compile(r'\[\[\[endsection\s*.*\]\]\]')

class SectionedFile(object):
    """Loads a file into memory and keeps track of all of the
    sections found in the file. Sections are started with a
    line that looks like this::
    
      [[[section SECTIONNAME]]]
    
    Anything else can appear on the line outside of the brackets
    (so if you're in a source code file, you can put the section marker
    in a comment). The entire lines containing the section markers are
    not included when you request the text from the file.
    
    An end of section marker looks like this::
    
      [[[endsection]]]
      
    Sections can be nested. If you do nest sections, you will use
    dotted notation to refer to the inner sections. For example,
    a "dessert" section within an "order" section would be referred
    to as "order.dessert".
    
    The SectionedFile provides dictionary-style access to the
    sections. If you have a SectionedFile named 'sf',
    sf[sectionname] will give you back a string of that section
    of the file, including any inner sections. There won't
    be any section markers in that string.
    
    You can get the text of the whole file via the ``all`` property
    (for example, ``sf.all``).
    
    Section names must be unique across the file, but inner section
    names are kept track of by the full dotted name. So you can
    have a "dessert" section that is contained within two different
    outer sections.
    
    Ending a section without starting one or ending the file without
    ending a section will yield BuildFailures.
    """
    
    def __init__(self, filename=None, from_string=None):
        """Initialize this SectionedFile object from a file or string.
        If ``from_string`` is provided, that is the text that will
        be used and a filename is optional. If a filename is provided
        it will be used in error messages.
        """
        self.filename = filename
        self.contents = []
        self.sections = {}
        if from_string is not None:
            from six import StringIO
            self._read_file(StringIO(from_string))
        else:
            with open(filename) as f:
                self._read_file(f)
        
    def _read_file(self, f):
        """Do the work of reading the file."""
        contents = self.contents
        sections = self.sections
        real_lineno = 1
        output_lineno = 0
        
        stack = []
        line = f.readline()
        while line:
            m = _sectionmarker.search(line)
            if m:
                section = m.group(1)
                debug(f"Section {section} found at {real_lineno} ({output_lineno})")
                stack.append(section)
                sectionname = ".".join(stack)
                if sectionname in sections:
                    raise BuildFailure("""section '%s' redefined
(in file '%s', first section at line %s, second at line %s)""" %
                                        (sectionname, self.filename,
                                         sections[sectionname][0],
                                         real_lineno))
                sections[".".join(stack)] = [real_lineno, output_lineno]
            elif _endmarker.search(line):
                sectionname = ".".join(stack)
                try:
                    section = stack.pop()
                except IndexError:
                    raise BuildFailure(f"End section marker with no starting marker (in file '{self.filename}', at line {real_lineno})")
                debug(f"Section {section} end at {real_lineno} ({output_lineno})")
                sections[sectionname].append(output_lineno)
            else:
                contents.append(line)
                output_lineno += 1
            line = f.readline()
            real_lineno += 1
        if stack:
            section = ".".join(stack)
            raise BuildFailure("""No end marker for section '%s'
(in file '%s', starts at line %s)""" % (section, self.filename, 
                                        sections[section][0]))
    
    def __getitem__(self, key):
        """Look up a section, and return the text of the section."""
        try:
            pos = self.sections[key]
        except KeyError:
            raise BuildFailure("No section '%s' in file '%s'" %
                               (key, self.filename))
        return "".join(self.contents[pos[1]:pos[2]])
    
    def __len__(self):
        """Number of sections available in the file."""
        return len(self.sections)
    
    def keys(self):
        return self.sections.keys()
    
    @property
    def all(self):
        """Property to get access to the whole file."""
        return "".join(self.contents)

_default_include_marker = dict(
    py="# "
)

class Includer(object):
    """Looks up SectionedFiles relative to the basedir.
    
    When called with a filename and an optional section, the Includer
    will:
    
    1. look up that file relative to the basedir in a cache
    2. load it as a SectionedFile if it's not in the cache
    3. return the whole file if section is None
    4. return just the section desired if a section is requested
    
    If a cog object is provided at initialization, the text will be
    output (via cog's out) rather than returned as
    a string.
    
    You can pass in include_markers which is a dictionary that maps
    file extensions to the single line comment character for that
    file type. If there is an include marker available, then
    output like:
    
    # section 'sectionname' from 'file.py'
    
    There are some default include markers. If you don't pass
    in anything, no include markers will be displayed. If you
    pass in an empty dictionary, the default ones will
    be displayed.
    """
    def __init__(self, basedir, cog=None, include_markers=None):
        self.include_markers = {}
        if include_markers is not None:
            self.include_markers.update(_default_include_marker)
        if include_markers:
            self.include_markers.update(include_markers)
        self.basedir = Path(basedir)
        self.cog = cog
        self.files = {}
    
    def __call__(self, fn, section=None):
        f = self.files.get(fn)
        if f is None:
            f = SectionedFile(self.basedir / fn)
            self.files[fn] = f
        ext = Path(fn).suffix.replace(".", "")
        marker = self.include_markers.get(ext)
        if section is None:
            if marker:
                value = marker + "file '" + fn + "'\n" + f.all
            else:
                value = f.all
        else:
            if marker:
                value = marker + "section '" + section + "' in file '" + fn \
                      + "'\n" + f[section]
            else:
                value = f[section]
        if self.cog:
            self.cog.cogmodule.out(value)
        else:
            return value