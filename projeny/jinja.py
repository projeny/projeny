"""
Utilities for Jinja
"""
import jinja2


def create_env(path):
    """
    Create a Jinja env for a filesystem path 
    """
    return jinja2.Environment(
            loader=jinja2.FileSystemLoader(path),
            trim_blocks=True,
            lstrip_blocks=True, 
        )

def collect_macros(jinja_env):
    """
    Returns dictionary of all macros in Jinja environment
    """
    
    macros = {}
    for template_file in jinja_env.list_templates():
        for name, block in jinja_env.get_template(template_file).module.__dict__.items():
            if isinstance(block,jinja2.runtime.Macro):
                macros[name] = block

    return macros
