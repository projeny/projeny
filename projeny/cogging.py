"""
Tools for annotating sections in a file.  The sections can be included verbatim in other files.

Taken from https://github.com/paver/paver/blob/master/paver/doctools.py
"""

from pathlib import Path
import os
import cogapp

class cogify(object):

    def __init__(self, f):
        """
        If there are no decorator arguments, the function
        to be decorated is passed to the constructor.
        """
        self.f = f

    def __call__(self, *args,**kwargs):
        """
        The __call__ method is not called until the
        decorated function is called.
        """
        r = self.f(*args,**kwargs)
        try:
            import cog
            cog.out(r)
        except Exception as e:
            print(f"Error calling {self.f.__name__}: {e}")
        return r

def _cogsh(cog):
    """The sh command used within cog. Runs the command (unless it's a dry run)
    and inserts the output into the cog output if insert_output is True."""
    def shfunc(command, insert_output=True):
        output = sh(command, capture=insert_output)
        if insert_output:
            cog.cogmodule.out(output)
    return shfunc

def _runcog(options: dict, uncog=False):
    """Common function for the cog and runcog tasks."""
    # Configure Cog to use replace and dont delete

    c = cogapp.Cog()
    if uncog:
        c.options.bNoGenerate = True
    c.options.bReplace = True
    c.options.bDeleteCode = options.get("delete_code", False)

    if 'functions' in options:
        for name, f in options['functions'].items():
            c.options.defines[name] = cogify(f)
    
    c.options.defines.update(options.get("defines", {}))

    # Select the snippet specification to search for
    c.options.sBeginSpec = options.get('beginspec', '[[[cog')
    c.options.sEndSpec = options.get('endspec', ']]]')
    c.options.sEndOutput = options.get('endoutput', '[[[end]]]')
    
    # Select which directory to search
    basedir = options.get('basedir', '.')
    basedir = Path(basedir)
        
    # Collect all the files that match the glob patter
    pattern = options.get("pattern", "*")
    files = basedir.glob(pattern)
    # Enable debugging output
    c.options.verbosity = 2

    # remove files in .gitignore
    
    gitignore_files = options.get('gitignore')
    if gitignore_files:
        files = set(files) - set(gitignore_files)
        
    # Run Cog on each file
    for f in files:
        fname = str(f)
        if '.projeny' in fname: continue
        print(f'Processing file {os.path.join(os.getcwd(),fname)}')
        c.processOneFile(fname)
    

def cog(options):
    """Runs the cog code generator against the files matching your 
    specification. By default, cog will run against any .rst files
    in your Sphinx document root. Full documentation for Cog is
    here:
    
    https://nedbatchelder.com/code/cog/
    
    In a nutshell, you put blocks in your file that look like
    this:
    
    [[[cog cog.outl("Hi there!")
    ]]]
    [[[end]]]
    
    Cog will replace the space between ]]] and [[[end]]] with
    the generated output. In this case, Hi there!
    
    Here are the options available for the cog task. These are
    looked up in the 'cog' options section by default. The
    'sphinx' option set is also searched.
    
    basedir
        directory to look in for files to cog. If not set,
        'docroot' is looked up.
    pattern
        file glob to look for under basedir. By default, ``*.rst``
    includedir
        If you have external files to include in your
        documentation, setting includedir to the root
        of those files will put a paver.doctools.Includer 
        in your Cog namespace as 'include'. This lets you
        easily include files and sections of files. Here's
        an example usage::

            [[[cog include('filename_under_includedir.py', 'mysection')]]]
            [[[end]]]
    defines
        Dictionary of objects added to your Cog namespace.
        (can supersede 'include' and 'sh' defined by includedir.)
    beginspec
        String used at the beginning of the code generation block.
        Default: [[[cog
    endspec
        String used at the end of the code generation block.
        Default; ]]]
    endoutput
        String used at the end of the generated output
        Default: [[[end]]]
    delete_code
        Remove the generator code. Note that this will mean that the
        files that get changed cannot be changed again since the code
        will be gone. Default: False
    include_markers
        Dictionary mapping file extensions to the single line
        comment marker for that file. There are some defaults.
        For example, 'py' maps to '# '. If there is a known
        include marker for a given file, then a comment
        will be displayed along the lines of:
        
        # section 'SECTIONNAME' in file 'foo.py'
        
        If this option is not set, these lines will not
        be displayed at all. If this option is set to an
        empty dictionary, the default include markers
        will be displayed. You can also pass in your own
        extension -> include marker settings.
    """
    from cogapp.cogapp import CogError
    try:
        _runcog(options)
    except CogError as e:
        # Handle error caused by renameing a directory with cog
        if "Can't overwrite" in str(e):
            print(f'Hit error {e}, rerunning')
            _runcog(options)
        else:
            raise
#@task
def uncog(options):
    """Remove the Cog generated code from files. Often, you will want to
    do this before committing code under source control, because you
    don't generally want generated code in your version control system.
    
    This takes the same options as the cog task. Look there for
    more information.
    """
    _runcog(options, True)


class ShellError(Exception):
    pass

def sh(command, capture=False, ignore_error=False, cwd=None, env=None):
    """Runs an external command. If capture is True, the output of the
    command will be captured and returned as a string.  If the command
    has a non-zero return code raise a BuildFailure. You can pass
    ignore_error=True to allow non-zero return codes to be allowed to
    pass silently, silently into the night.  If you pass cwd='some/path'
    paver will chdir to 'some/path' before exectuting the command.
    If the dry_run option is True, the command will not
    actually be run.
    
    env is a dictionary of environment variables. Refer to subprocess.Popen's
    documentation for further information on this."""
    import subprocess, shlex, sys
    _shlex_quote = shlex.quote

    error = print

    if isinstance(command, (list, tuple)):
        command = ' '.join([_shlex_quote(c) for c in command])

    def runpipe():
        kwargs = {'shell': True, 'cwd': cwd, 'env': env}
        if capture:
            kwargs['stderr'] = subprocess.STDOUT
            kwargs['stdout'] = subprocess.PIPE
        p = subprocess.Popen(command, **kwargs)
        p_stdout = p.communicate()[0]
        if p_stdout is not None:
            p_stdout = p_stdout.decode(sys.getdefaultencoding(), 'ignore')
        if p.returncode and not ignore_error:
            if capture and p_stdout is not None:
                error(p_stdout)
            raise ShellError("Subprocess return code: %d" % p.returncode)

        if capture:
            return p_stdout

    return runpipe()