import box
import pathlib 
class Functions(box.Box):
    
    @classmethod
    def from_pyfile(cls, basepath: pathlib.Path, pyfile: pathlib.Path):
        import importlib, sys, inspect

        sys.path.insert(0, str(basepath))
        module = importlib.import_module(pyfile.stem)
    
        # get the functions in that module
        name_func = {
            name: function
            for name, function in inspect.getmembers(module, inspect.isfunction)
        }

        return cls(name_func,frozen_box=True)
