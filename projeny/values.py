import box
import ruamel
import os

class Values(box.Box):
    pass

def Loader(basepath):

    def ctor_openapi(loader, node):
        """
        PyYAML Constructor that parses and validates OpenAPI spec
        """
        fpath = loader.construct_scalar(node)
        import prance
        if not os.path.isabs(fpath):
            fpath = os.path.normpath(os.path.join(basepath, fpath))
        return prance.ResolvingParser(fpath).specification

    loader = ruamel.yaml.SafeLoader
    loader.add_constructor(u'!openapi', ctor_openapi)
    return loader