"""
Projeny Project

* Encapsulates access to the projeny directory, configuration, templates and values
"""

import os
import pathlib
import shutil
from projeny import cogging, jinja, values as pvalues, config, functions

PROJENY_DIR_NAME = ".projeny"
PROJENY_CONFIG_YAML = "config.yaml"
PROJENY_VALUES_YAML = "values.yaml"
PROJENY_TEMPLATES_DIR_NAME = "templates"
PROJENY_FUNCTIONS = "functions.py"


class Project(object):
    def __init__(
        self,
        root,
        projeny_dir=PROJENY_DIR_NAME,
        values_file=PROJENY_VALUES_YAML,
        config_file=PROJENY_CONFIG_YAML,
        templates_dir=PROJENY_TEMPLATES_DIR_NAME,
        functions_file=PROJENY_FUNCTIONS,
    ):

        projeny_dir = find_projeny_dir(root)
        self._projeny_dir = projeny_dir

        # Setup the jinja templates environment
        self.jinja_env = jinja.create_env(projeny_dir / templates_dir)

        # Get the functions to use globaly in project
        self._functions = {}
        if (projeny_dir / functions_file).exists():

            # import functions file as a module
            self._functions = functions.Functions.from_pyfile(
                projeny_dir, (projeny_dir / functions_file)
            )

            # add the functions to jinja environment as filters
            self.jinja_env.filters.update(self._functions)

        # Get the cogapp configuration for this project
        self._config = {}
        if (projeny_dir / config_file).exists():
            self._config = config.Config.from_yaml(filename=(projeny_dir / config_file),frozen_box=False)

        # Store jinja macros so they can be automatically imported
        # * WARNING* Do this only after the jinja environment is fully configured.
        self._macros = jinja.collect_macros(self.jinja_env)

        self._gitignore_files = []

    def _values_from_file(self,file,frozen=True):
        # Get the values for this project and add to jinja environment
        self._values = {}
        values_file = self.projeny_file(file)
        if values_file.exists():
            self._values = pvalues.Values.from_yaml(
                filename=values_file,
                Loader=pvalues.Loader(basepath=self.root),
                frozen_box=frozen
            )
            self.jinja_env.globals["values"] = self._values
        return self._values

    def copy_values(self,) -> pvalues.Values:
        return self._values_from_file(PROJENY_VALUES_YAML,frozen=False).copy()

    @property
    def values(self) -> pvalues.Values:
        return self._values_from_file(PROJENY_VALUES_YAML,frozen=True)
    
    @values.setter
    def values(self,d) -> None:
        v = self.copy_values()
        v.merge_update(d)
        v.to_yaml(filename=self.projeny_file(PROJENY_VALUES_YAML))

    @property
    def config(self) -> config.Config:
        return self._config

    @property
    def functions(self) -> functions.Functions:
        return self._functions

    @property
    def root(self):
        return self._projeny_dir.parent

    def projeny_file(self, path):
        file = self._projeny_dir / path
        if not file.exists():
            raise Exception(f"Unable to find '{path}' in {self._projeny_dir}")

        return file

    def _get_template(self, name):
        return self.jinja_env.get_template(name=name)

    @property
    def gitignored_spec(self):
        gitignore = pathlib.Path(self.root,'.gitignore')
        if not gitignore.exists():
            return None
            
        with open(gitignore) as f:
            gitignore_spec = f.read()

        import pathspec
        spec = pathspec.PathSpec.from_lines(pathspec.patterns.GitWildMatchPattern, gitignore_spec.splitlines())
        return spec

    @property
    def patterns(self):
        return [c.get('pattern') for c in self.config.values()]

    @property
    def gitignored_files(self):
        if not self._gitignore_files:
            spec = self.gitignored_spec
            if spec:
                files = spec.match_tree(self.root)
                self._gitignore_files = [pathlib.Path(f) for f in files if f not in self.patterns]
        return self._gitignore_files

    def render(self, output,values=None,config=None,overwrite=False,eject=False):
        spec = self.gitignored_spec
        ignore_dirs = ['.git']
        def _ignore(src,names):
            if not spec:
                return []
            relative_to_project_root = [(pathlib.Path(src) / pathlib.Path(n)).relative_to(self.root) for n in names]
            relative_to_project_root = [n for n in relative_to_project_root if str(n) not in self.patterns]
            ignored = spec.match_files(relative_to_project_root)
            relative_to_src =  [(self.root / i).relative_to(src) for i in ignored]

            ignored_names = [str(s) for s in relative_to_src]
            for n in names:
                if n in ignore_dirs and n not in ignored_names:
                    ignored_names.append(n)
            return ignored_names

        if os.path.exists(output) and overwrite:
            shutil.rmtree(output)

        shutil.copytree(self.root, output,ignore=_ignore)
        copied_project = Project(output)
        if values is not None:
            #values_file = copied_project.projeny_file('values.yaml')
            #with open(values_file, 'w') as f:
            #    import yaml
            #    yaml.dump(values,f)
            copied_project.values = values
            #values_file = copied_project.projeny_file('values.yaml')
            #with open(values_file, 'w') as dst:
            #    with open(self.projeny_file('values.yaml'),'r') as src:
            #        dst.write(src.read())
                    
        copied_project._render(configs=config,eject=eject)
        return copied_project

    def jinja(self,template_str: str,**context):
        template = self.jinja_env.from_string(template_str, globals=self._macros)
        return template.render(**context)

    def _render(self,configs=None,eject=False,use_gitignore=True):
        """ Render the project -- runs cog """
        cwd = os.getcwd()
        os.chdir(self.root)

        try:
            values = self.values

            for config_name, options in self.config.items():
                if configs and config_name not in configs:
                    continue 
                print(f"---Running code generator for {config_name}---")
                options["defines"] = {
                    "values": values,
                    "project" : self
                }
                options["functions"] = {
                    "jinja": self.jinja,
                    "p": lambda s: s,
                    **self.functions,
                    **{name: macro._func for name, macro in self._macros.items()},
                }
                options['delete_code'] = eject
                if use_gitignore:
                    options['gitignore'] = self.gitignored_files
                cogging.cog(options)
        finally:
            os.chdir(cwd)


    def hide(self, path, when=True):
        """
        moves a file or directory into the projeny directory's 'hidden' folder.  this can be undone by calling 'unhide'
        """

        if when == False and self.is_hidden(path):
            self.unhide(path)
            return

        old_path = self.root / path
        new_path = self.hidden_dir / path
        
        if not old_path.exists():
            if new_path.exists():
                return
            else:
                raise FileNotFoundError(f"Trying to hide path that doesn't exist: {old_path}")

        if not old_path.is_dir():
            os.makedirs(str(new_path.parent),exist_ok=True)
            
        shutil.move(str(old_path),str(new_path))

    def list_hidden(self,absolute_paths=False, directories=False):
        """
        List of paths in the projeny directory's 'hidden' folder
        """
            
        # Get the list of all files in directory tree at given path
        listOfFiles = list()
        for (dirpath, dirnames, filenames) in os.walk(self.hidden_dir):
            listOfFiles += [os.path.join(dirpath, file) for file in filenames]
            if directories:
                listOfFiles +=[dirpath]

        if not absolute_paths:
            listOfFiles = [str(pathlib.Path(f).relative_to(self.hidden_dir)) 
                            for f in listOfFiles]

            listOfFiles = list(filter(lambda f:f!='.',listOfFiles))
        return listOfFiles

    @property
    def hidden_dir(self):
        return self._projeny_dir / 'hidden'

    def is_hidden(self,path):
        """Check if file or path is hidden"""
        return path in self.list_hidden(directories=True)

    def unhide(self, path):
        """
        moves file or directory from 'hidden' folder back to the main project. If path is not provided, all paths are unhidden
        """
        
        if not self.is_hidden(path):
            return

        new_path = self.root / path
        old_path = self.hidden_dir / path
        
        if not old_path.exists():
            if new_path.exists():
                return
            else:
                raise FileNotFoundError(f"Trying to unhide a path that doesn't exist: {old_path}")

        if not old_path.is_dir():
            os.makedirs(str(new_path.parent),exist_ok=True)
            
        shutil.move(str(old_path),str(new_path))

        
def find_projeny_dir(path="."):
    """
    Search directories updward until`.projeny` is found
    """

    cdir = pathlib.Path(path).absolute()
    if not cdir.is_dir():
        raise FileNotFoundError(f"The path {path} is not a directory")
    cwd = cdir
    while not (cdir / PROJENY_DIR_NAME).is_dir():
        if cdir == pathlib.Path("/"):
            raise Exception(
                f"Unable to find '{PROJENY_DIR_NAME}' directory in '{cwd}'' or its parent directories"
            )
        cdir = cdir.parent
    return cdir / PROJENY_DIR_NAME