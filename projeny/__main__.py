from projeny import project

def main():
    
    import sys
    target = '.'
    if len(sys.argv) == 2:
        target = sys.argv[1]
    if target == '.':
        project.Project('.')._render()
    else:
        project.Project('.').render(target,overwrite=True)
if __name__ == '__main__':
    main()