#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

import io
import re
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    with io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ) as fh:
        return fh.read()


setup(
    # [[[cog 
    # jinja(
    # """name='{{values.project.name}}',
    # description='{{values.project.description}}',
    # version='{{values.project.version}}',
    # license='{{values.project.license}}',
    # author='{{values.company.name}}',
    # author_email='{{values.company.email}}',
    # url='{{values.repo.url}}',
    # project_urls={
    #    'Changelog': '{{values.repo.url}}/blob/master/CHANGELOG.rst',
    #    'Issue Tracker': '{{values.repo.url}}/issues',
    # },
    # """)
    # ]]] 
    name='Projeny',
    description='Project generation for humans',
    version='0.0.1',
    license='BSD-2-Clause',
    author='The Projeny Team',
    author_email='incoming+ali1rathore-cookiecutter-flask-13461257-issue-@incoming.gitlab.com',
    url='https://gitlab.com/projeny/projeny',
    project_urls={
       'Changelog': 'https://gitlab.com/projeny/projeny/blob/master/CHANGELOG.rst',
       'Issue Tracker': 'https://gitlab.com/projeny/projeny/issues',
    },
    # [[[end]]]
    long_description='%s\n%s' % (
        re.compile('^.. start-badges.*^.. end-badges', re.M | re.S).sub('', read('README.md')),
        re.sub(':[a-z]+:`~?(.*?)`', r'``\1``', read('CHANGELOG.md'))
    ),
    packages=['projeny'],  #find_packages('projeny'),
    #package_dir={'': 'projeny'},
    #py_modules=[splitext(basename(path))[0] for path in glob('projeny/*.py')],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        # uncomment if you test on these interpreters:
        # 'Programming Language :: Python :: Implementation :: IronPython',
        # 'Programming Language :: Python :: Implementation :: Jython',
        # 'Programming Language :: Python :: Implementation :: Stackless',
        'Topic :: Utilities',
    ],
    keywords=[
        # eg: 'keyword1', 'keyword2', 'keyword3',
    ],
#    python_requires='>=3.8.*',
    install_requires=[
        'jinja2',
        'cogapp',
        'python-box',
        'prance[osv]',
        'ruamel.yaml',
        "openapi-spec-validator>=0.2.9",
        "pathspec"
    ],
    extras_require={
        # eg:
        #   'rst': ['docutils>=0.11'],
        #   ':python_version=="2.6"': ['argparse'],
    },
    setup_requires=[
        'pytest-runner',
    ],
    entry_points={
        'console_scripts': [
            'projeny = projeny.__main__:main',
        ]
    },
)
