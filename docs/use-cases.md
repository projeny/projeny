# Product and Market

## Generate a Flask Apps

Create Flask Apps with these features:

#### Non-Application-Domain
* Auth
* Docker / Docker-Compose
* Serverless Deployment
* Database (use stuff from fastapi)
* VSCode
* PyTest
* CLI
* PIP
* Docs
* Blog

#### Applications
* Chat
* ToDo
* 

## Generate Basic Python Project

* VSCode
* PyTest
* CLI
* PIP

## Generate Data/ML Python Project

#### Non Domain

* VSCode
* Jupyter Notebooks/Lab
* Scientific Libraries

#### Application

