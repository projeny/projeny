# Enabling new sets of functionality 

Key Goals: 

* each functionality should be completely runnable and testable on its own.
* functionality should be downloadable as a seperate package
* each functionality can optionality depend on other functionalities

## Running Example:

For example, we want to add serverless capabilities to our app. We need to do the
following steps:

1. Add serverless installation to the Dockerfile
2. Ceate the serverless.yaml file depending on configuration, inject ENV vars etc.
3. If Sentry is enabled, add the AWSLambda plugin for serverless.
4. Add documentation for how to use and configure

## Approach 0 - dont try to user any plugin-like system, just hardcode all conditions into pre-created files.

Create all the possible files that may exist, and remove them at ejection if not needed


## Approach 1 - use plugin system with callbacks to generate needed code

We can do all of these by adding just one new file to our .projeny/plugins

```python
### .projeny/plugins/serverless.py

class Serverless():
    
    def plugins():
        return [SentryServerless,InstallNPM]
    
    def Dockerfile(dockerfile):
        if 'npm install serverless' in dockerfile:
            return
            
        return '''
        npm install serverless
        '''

    def README(readme):
    
        return '''
        # Deploying with Serverless
        
        This project uses serverless to deploy
        '''
        
    @files.create('serverless.yaml')
    def create_serverless(model):
        return '''
        version ...
        provider ...
        '''

```

## Approach 2 - Inject files where needed

Keep new files and snippets in directory structure `.projeny/components/serverless`

* Add to README.md

```md
<!--[[projeny block('README.md/#/Deployment/Serverless') ]]-->
# Usage with Serverless

You can use this project in serverless deployment:

`serverless deploy `
<!--[[end]]-->
```

* Add to Dockerfile

```Dockerfile
FROM ubuntu:14.04

#[[projeny block('Dockerfile/#/apt-install/npm')]]
apt-get install npm
#[[end]]

```

* Add serverless.yaml

In the `.projeny/components/serverless/serverless.yaml

```yaml
#[[projeny block('serverless.yaml/#')]]

version: 1.0
provider:
    aws: ...

functions:
    ...

#[[end]]
```


# Approach 3 - use templates and single yaml file

Use a single yaml file to describe where different files should be generated


```yaml
### .projeny/components/serverless/.projeny.yaml

files:
    README.md/#/Deployment:
        destinations:
            - /README.md/#/Deployment/Serverless
            - /docs/sections/serverless/README.md
    
    Dockerfile/#/apt-install/npm:
        destinations:
            - /Dockerfile/#/apt-install/npm
    
    
    
```

pros: 
cons: maybe using file/paths is not greate idea. instead replace with teplate names


## Approach use something that looks like hyperlinks

 TODO
 
 
# Another Example: Flask-Admin

Requires following steps:

1. add to requirements.txt
2. add 'admin.py' to each blueprint: `cp .projeny/flask/admin.py application/blueprints/todo/admin.py
  or : `projeny flask/admin.py todo` or `projeny flask/admin.py todo/openapi.yaml/#/TODO

