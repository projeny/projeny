# Getting Started

To start using projeny, create a directory called `.projeny` in your projects root directory.

This directory will contain:

* `values.yaml` : a yaml (or json) file that contains the values you want to inject into various parts of your project.  See the `.projeny/values.yaml` file in this project for an example.

* `config.yaml`: a yaml file that contains
instructions about how to run the code generator
on different files in your project

* `templates` : Jinja2 templates and macros that you want to be rendered in different places in your project.
