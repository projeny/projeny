<!--| p("# " + values.project.name) |-->
# Projeny
<!--|end|-->

<!--| p(values.project.description) |-->
Project generation for humans
<!--|end|-->

## Quickstart

<!--| 
jinja("""
Run the following commands to bootstrap your environment

    git clone {{ values.repo.url }}
    cd {{ values.repo.name }}
    pip install -r requirements/dev.txt [--ignore-installed]
    cp .env.example .env
    npm install
    npm start  # run the webpack dev server and flask server using concurrently
""")     
|-->

Run the following commands to bootstrap your environment

    git clone https://gitlab.com/projeny/projeny
    cd projeny
    pip install -r requirements/dev.txt [--ignore-installed]
    cp .env.example .env
    npm install
    npm start  # run the webpack dev server and flask server using concurrently
<!--|end|-->