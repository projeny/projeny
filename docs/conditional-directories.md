# Managing files

Sometimes you want to conditionally remove files or directories as part of a feature.

For example, how can we add the `serverless.yaml` file if the feature
is enabled?

---
## Option 1: Use a Cog function

The file starts with a specific line that can remove that file

For example. take this `serverless.yaml` file:

```yaml
# [[[cog if not values.features.serverless.enabled: rm() ]]]
provider:
    aws:...

functions:
# [[[cog
# if sentry.enabled:
#   print("SENTRY: ")....
# ]]]
# [[[end]]]
    api:
        ...
```

When projeny is ran, this would save the `serverless.yaml` file in
`.projeny/saved/serverless.yaml`

Directories could be done simmilarly.  For example a test directory `server/tests/auth` could be conditionally removed by adding a new function.

Lets say we have a file `test_login.py`

```python
# [[[cog if not values.features.auth.enabled: rmdir() ]]]
def test_login(this=that):
    assert this != that
```

This would save the entire directory to `.projeny/saved/server/tests/auth/`

## Restoring removed files/directories

Upon running `projeny apply`, all files/directories are copied from `.projeny/saved` to their respective location, then `cog` is run as usual and re-remove the file is configured to do so.