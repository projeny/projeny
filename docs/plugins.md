# Projeny Plugins

Projeny plugins basically encapsulate code-generation use-cases.

Some plugin examples are:

* projeny-gitlab-ci : generates `gitlab-ci.yml` file for adding GitLab CI.
* projeny-gitlab-pages : generated the `public` directory needed for GitLab pages.
* projeny-pypackage : generates `setup.py` file for creating python package.
* projeny-flask : generates flask project files e.g `app.py`, `blueprints/`, `config/`
* projeny-changelog : generates `CHANGELOG.md` files

A plugin in a `projeny` project provides additonal:

* templates/ - Set of templates
* config.yaml - configuration for the rendering e.g the cogapp options
* values.yaml - the model and values used in rendering
* functions.py - Functions used durring rendering

