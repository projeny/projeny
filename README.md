# projeny - Automate your coding project

With projeny, you can:

* Auto-complete your README.md with the project's name, description, contributors, links etc
* Auto-complete your installer scripts (e.g setup.py) with git version, requirements etc.
* Auto-complete all your boilerplate (SQL model, HTML Forms, etc) from schema specs
* Do all of the above at anypoint in the project's lifetime, not just in the beginning
* Inject text snippets anywhere in your projects.
* Make parts of your code re-usable across different projects, without having to create a package

## Use Cases:

* Generate brand new project from an existing project - replaces cookie-cutter / yeoman
* Generate models, routes, docs, html code from OpenAPI spec - replaces home-made hacks
* Global rebranding of a project - rebrand your project for different customers
* Generate in-line code anywhere in your project - replaces runtime feature-flags
* Convert existing `cookie-cutter` into a usable project
* copy and paste snippets from one file to another

### Key features:
* project's metadata (users, repo, name, etc) can be changed at any time
* various build-in tools for managing project components (banner, links, docs, etc)
* no template files in your project repository
* uses jsonschema to specify the project's metadata
* simple plugin system for addig prepackaged sets of functionality to your project

### Alternatives

* cookie-cutter
* yeoman

# Getting Started

### Installation

```bash
# Install using pip
pip install projeny
```

Now in our project directory, setup projeny

```bash
$ cd <path/to/project/root>

$ projeny init # this will create the ./.projeny directory
```

```bash
commands

init - setup new projeny directory
apply - generate all the code (will uneject first - undo the previous eject - put back all projeny-snippets)
eject - keep all generated code, remove all projeny-snippets
edit - open editor to project configuration

```

# Examples

### Setup the Projeny directory

```txt
### .projeny

./project/ - project-wide items
   variables.yaml - global variables
   functions.yaml - global functions
   __init__.py - global variables (project name, description, etc.)
   
./flask/ - flask related items
   | __init__.py
   | requirements.py
   | blueprints.py
   | templates/
        | WTForm.py
   | ...
   | tests_views.py
   | test_models.py
   
./mkdocs/ - mkdocs related items
```

### Generate pip requirements
```requirements

### requirements.txt

thismodueldoesntexist==4.3

#[[projeny requirements()]]
#[[end]]

```

### Generate the README.md
```README.md

### README.md

#[[projeny banner()]]
#[[end]]

#[[projeny 
#  print(f"You can find the links for {project.name} here: {','.join(project.links}")
#]]
You can find the links for Projeny here: 'this.com', 'that.com'
#[[end ]]

```

### Generate Flask App

```python

### app.py


from flask import Flask

#[[projeny
# for b in project.flask.blueprints():
#   print(f"app app.blueprints import {b}")
#]]


#[[end]]

app = Flask(__name__)

#[[projeny
# for b in project.flask.blueprints():
#     print(f"app.register_blueprint(b)"
#]]

```

### Generate Flask Form
```python

### app/blueprints/todo/model.py


class TodoForm(WTForm):

#   [[projeny render(./openapi.yaml/#components/schema/Todo/properties, 'FlaskWTForms') ]]
#   [[end]]


#  OR????

#   [[projeny flask.WTForm('./openapi.yaml/#components/schema/Todo/properties') ]]
#   [[end]]

#  OR????

#   [[projeny flask.WTForm('./openapi.yaml/#components/schema/Todo').properties() ]]
#   [[end]]

```

### Edit the FlaskWTForm generator

```python
### .projeny/flask/__init__.py
import projeny
import jinja2

def _generate_form(model_path):

  model = projeny.get_openapi_model(model_path)
  template = jinj2.template('./FlaskWTF.py')
  
  return template.render(context=model)
  
def WTForm(model_path):
    print(_generate_form(model_path))
    
# OR??????

class OpenSchemaRenderer(object):
    def name():
        pass
    def properties():
        pass
    

class WTForm(OpenAPIRender):

    template = 'FlaskForm.py'
        



```

### Edit the FlaskWTForm Templates

```jinja2
### .projeny/flask/templates/FlaskForm.py

from flask_wtf import FlaskForm

{% block schema %}
def {{model.name}}(FlaskForm):
  {% block properties %}
    {% for field in model.properties %}
        {{ field.name }} = {% block type %}{{ field.type }}{% endblock %}
    {% endfor }
  {% endblock %}
{% endblock %}
```

# Projeny Server

The projeny app is a web app that you can use in your project to help
with creating templates and models

Start the projeny server: `projeny server --port 8081`

You can see the contents of the .projeny directory in the browser.
From here you can see the templates, the models, and the rendered results for a given schema

### Remote Projeny

Sometimes is would be helpful to keep the projeny templates and generator functions
in a remote repo.  You can use the projeny server to centrialize the generation

Use Cases:
  * Keep the templates in a centrialized location for all your projects
  * Secure the code generation behing an authenticated api
    * User's can submit the model and get the rendered files, they cannot see the templates

# Questions

#### Using Projeny for feature-flags

A feature-flag is a variable in the code that enables or disables a certain feature. With
Projeny, you can create feature flags without adding additional runtime code.  Instead,
the feature-flag can be configured when your application is built, deployed, or started.


##### Adding a feature-flag:

1. create a new directory `new-feature` under `.projeny/features`
2. edit `.projeny/project.yaml`

```yaml
### .projeny/project.yaml

metadata:
  name: thisproject
  author: thisperson
  version: newversion
  
features:
  serverless: true
  flask-sqlalchemy: true
  sentry: true
  mkdocs: true
  new-feature: true
  
 

```

3. add files with snippets under the new `.projeny/features/new-feature` directory

* README.md
* serverless.yaml
* Dockerfile
* app.py

**OOOOORRRRR**

4. add all

#### How do we push to repo without any projeny-snippets

Do not commit the projeny-snippets, this is usefull for keeping your repo clean.

For example, use this for your README.md because that file cannot have comments.

**Ideas:** 

* Could this be handled by using a separate branch?
* Can we use git-precommit hook to eject, commit, uneject?.  After fetch, apply again
  * ./projeny/.nocommit - list of blobs to not comit
* Can we use hidden file? 
  * ./projeny/files/README.md -> generates /README.md after "apply"
  * ./projeny/files/app/blueprint/some-text-file.txt -> generates /app/blueprint/some-text-file.txt

#### How to put projeny-snippets in markdown files?

#### How to do conditional file-creation
* The existance of certain files will depend on the project model. For Example:
  * They will only have a 'serverless.yaml' file if using serverless. 
  * They will only have a 'docker-compose.yaml' file if using docker-compose
  