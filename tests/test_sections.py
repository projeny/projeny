import sys
from pathlib import Path
from projeny import sections

# taken from https://github.com/paver/paver/blob/master/paver/tests/test_doctools.py

def test_sections_from_file():
    simpletext = """# [[[section foo]]]
#Foo!
# [[[endsection]]]
"""
    f = sections.SectionedFile(from_string=simpletext)
    assert len(f) == 1, "Sections found: %s" % f
    assert f['foo'] == "#Foo!\n", "Foo section contained: '%s'" % f['foo']

def display(msg, *args):
    print(msg % args)

sections.debug = display

def test_nested_sections():
    myfile = """
[[[section bar]]]
    Hi there.
    [[[section baz]]]
    Yo.
    [[[endsection]]]
[[[endsection]]]
"""
    f = sections.SectionedFile(from_string=myfile)
    assert len(f) == 2
    assert f['bar'] == """    Hi there.
    Yo.
""", "Bar was: '%s'" % (f['bar'])
    assert f['bar.baz'] == """    Yo.
"""

def test_section_doesnt_end():
    myfile = """
[[[section bar]]]
Yo.
"""
    try:
        sections.SectionedFile(from_string=myfile)
        assert False, "Expected a BuildFailure"
    except sections.BuildFailure:
        e = sys.exc_info()[1]
        assert str(e) == """No end marker for section 'bar'
(in file 'None', starts at line 2)""", "error was: %s" % (str(e))

def test_section_already_defined():
    myfile = """
[[[section foo]]]
First one.
[[[endsection]]]

[[[section foo]]]
Second one.
[[[endsection]]]
"""
    try:
        sections.SectionedFile(from_string=myfile)
        assert False, "Expected a BuildFailure"
    except sections.BuildFailure:
        e = sys.exc_info()[1]
        assert str(e) == """section 'foo' redefined
(in file 'None', first section at line 2, second at line 6)""", \
        "error was: %s" % (str(e))
        

def test_endmarker_without_start():
    myfile = """
[[[section foo]]]
This is a good section.
[[[endsection]]]

[[[endsection]]]
"""
    try:
        sections.SectionedFile(from_string=myfile)
        assert False, "Expected a BuildFailure"
    except sections.BuildFailure:
        e = sys.exc_info()[1]
        assert str(e) == """End section marker with no starting marker (in file 'None', at line 6)""", \
        "error was: %s" % (str(e))

def test_whole_file():
    myfile = """
[[[section bar]]]
    Hi there.
    [[[section baz]]]
    Yo.
    [[[endsection]]]
[[[endsection]]]
"""
    f = sections.SectionedFile(from_string=myfile)
    assert f.all == """
    Hi there.
    Yo.
""", "All was: %s" % (f.all)
    
def test_bad_section():
    f = sections.SectionedFile(from_string="")
    try:
        f['foo']
        assert False, "Should have a BuildFailure"
    except sections.BuildFailure:
        e = sys.exc_info()[1]
        assert str(e) == "No section 'foo' in file 'None'", \
               "Error: '%s'" % (str(e))
    
def test_include_lookup():
    basedir = Path(__file__).parent / "data"
    include = sections.Includer(basedir, include_markers={})

    everything = include("t1.py")
    assert everything == """# file 't1.py'
print("Hi!")

print("More")
""", "Everything was: '%s'" % (everything)

    first = include("t1.py", "first")
    assert first == """# section 'first' in file 't1.py'
print("Hi!")
""", "First was '%s'" % (first)

    second = include("t2.py", "second.inner")
    assert second == """# section 'second.inner' in file 't2.py'
print(sys.path)
""", "Second was '%s'" % (second)
