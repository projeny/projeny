import sys
from pathlib import Path
from projeny import cogging

from projeny.sections import Includer

# taken from https://github.com/paver/paver/blob/master/paver/tests/test_doctools.py

def test_cogging_includer():
    basedir = Path(__file__).parent

    options = { 
        'basedir': basedir,
        'pattern' : "*.rst",
    }

    options['functions'] = {
        'include' : Includer(basedir=basedir / 'data')
    }

    cogging.cog(options)

    textfile = basedir / "data" / "textfile.rst"
    with open(textfile) as f:
        data = f.read()

    assert "print(sys.path)" in data

    cogging.uncog(options)
    with open(textfile) as f:
        data = f.read()
    assert "print(sys.path)" not in data
    
def test_cogging_with_markers_removed():
    
    basedir = Path(__file__).parent

    options = { 
        'basedir': basedir,
        'pattern' : "*.rst",
        'delete_code' : True
    }

    options['functions'] = {
        'include' : Includer(basedir=basedir / 'data')
    }

    textfile = basedir / "data" / "textfile.rst"
    with open(textfile) as f:
        original_data = f.read()
    try:
        cogging.cog(options)
        with open(textfile) as f:
            data = f.read()
        assert "[[[cog" not in data
    finally:
        with open(textfile, "w") as f:
            f.write(original_data)


