
import pathlib
import shutil
import utils

from projeny.project import Project

project_path = pathlib.Path(__file__).parent / 'data' / 'basic-project'
expected_path = pathlib.Path(__file__).parent / 'expected' / 'basic-project-rendered'


def test_project_from_path():

    assert Project(project_path).root == project_path


def test_get_projeny_values():

    projeny_values = Project(project_path).values
    assert projeny_values['project']['name'] == 'BasicProject'
    assert projeny_values.project.name == 'BasicProject'


def test_get_projeny_config():

    projeny_config = Project(project_path).config

    assert projeny_config['docs']['basedir'] == 'basic-dir'
    assert projeny_config.docs.basedir == 'basic-dir'

def test_get_template():

    template = Project(project_path)._get_template('demo-template.txt')
    print(template.render())

    assert template.render() == "This is a demo template: Hello world! BasicProject"

def test_render_project(tmp_path):
    
    rendered = Project(project_path).render(output=(tmp_path / 'basic-project-rendered'))

    assert utils.are_dir_trees_equal(rendered.root, expected_path), \
                f"Trees are not equal {rendered.root} {expected_path}"

    shutil.rmtree(tmp_path)