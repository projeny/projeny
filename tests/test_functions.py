import pathlib
import shutil
import utils

from projeny import Project

project_path = pathlib.Path(__file__).parent / 'data' / 'flask-project'
expected_path = pathlib.Path(__file__).parent / 'expected' / 'flask-project-rendered'

def test_functions(tmp_path):
        
    rendered = Project(project_path).render(output=(tmp_path / 'flask-project-rendered'))

    assert utils.are_dir_trees_equal(rendered.root, expected_path), \
                f"Trees are not equal {rendered.root} {expected_path}"

    shutil.rmtree(tmp_path)

