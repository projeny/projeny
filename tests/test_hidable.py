import pathlib
import shutil
import utils
import pytest

from projeny import Project

project_path = pathlib.Path(__file__).parent / 'data' / 'hidable-project'
expected_path = pathlib.Path(__file__).parent / 'expected' / 'hidable-project-rendered'


def test_project_hide_directory(tmp_path):
    
    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'

    assert (tmp_project_path / 'hidable_directory').exists()
    project = Project(tmp_project_path)
    project.hide('hidable_directory')

    assert not (tmp_project_path / 'hidable_directory').exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' ).exists()

    shutil.rmtree(tmp_project_path)

def test_project_hide_file(tmp_path):
    
    shutil.copytree(project_path, tmp_path / 'hidable-project')

    tmp_project_path = tmp_path / 'hidable-project'

    assert (tmp_project_path / 'hidable_file.py').exists()
    project = Project(tmp_project_path)
    project.hide('hidable_file.py')

    assert not (tmp_project_path / 'hidable_file.py').exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_file.py' ).exists()

    shutil.rmtree(tmp_project_path)


def test_project_hide_deep_file(tmp_path):
    
    shutil.copytree(project_path, tmp_path / 'hidable-project')

    tmp_project_path = tmp_path / 'hidable-project'

    assert (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    project = Project(tmp_project_path)
    project.hide('hidable_directory/hidable_directory_file.py')

    assert not (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' / 'hidable_directory_file.py' ).exists()

    shutil.rmtree(tmp_project_path)

def test_project_hide_many(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)
    project.hide('hidable_directory/hidable_directory_file.py')
    project.hide('hidable_file.py')

    assert not (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    assert not (tmp_project_path / 'hidable_file.py').exists()

    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' / 'hidable_directory_file.py' ).exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_file.py' ).exists()

    shutil.rmtree(tmp_project_path)


def test_project_hide_many2(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    project.hide('hidable_file.py')
    project.hide('hidable_directory/hidable_directory_file.py')

    assert not (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    assert not (tmp_project_path / 'hidable_file.py').exists()

    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' / 'hidable_directory_file.py' ).exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_file.py' ).exists()


def test_project_hide_deep_file_then_dir(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    project.hide('hidable_directory/hidable_directory_file.py')

    assert not (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' / 'hidable_directory_file.py' ).exists()
    project.hide('hidable_directory')
    assert not (tmp_project_path / 'hidable_directory').exists()

    shutil.rmtree(tmp_project_path)

def test_project_hide_deep_file_then_dir(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    project.hide('hidable_directory/hidable_directory_file.py')

    assert not (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' / 'hidable_directory_file.py' ).exists()
    project.hide('hidable_directory')
    assert not (tmp_project_path / 'hidable_directory').exists()

    shutil.rmtree(tmp_project_path)

def test_project_hide_dir_then_deep_file(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    assert (tmp_project_path / 'hidable_directory').exists()
    assert (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    project.hide('hidable_directory')
    assert not (tmp_project_path / 'hidable_directory').exists()
    assert not (tmp_project_path / 'hidable_directory' / 'hidable_directory_file.py').exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' / 'hidable_directory_file.py' ).exists()
    assert (tmp_project_path / '.projeny' / 'hidden' / 'hidable_directory' ).exists()

    project.hide('hidable_directory/hidable_directory_file.py')

    shutil.rmtree(tmp_project_path)


def test_project_file_not_exist(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)
    with pytest.raises(FileNotFoundError):
        project.hide('this_file_doesnt_exist.py')
    shutil.rmtree(tmp_project_path)

def test_project_dir_not_exist(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)
    with pytest.raises(FileNotFoundError):
        project.hide('this_dir_does_not_exist')
    shutil.rmtree(tmp_project_path)

def test_list_hidden_files(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    project.hide('hidable_directory')
    project.hide('hidable_directory/hidable_directory_file.py')
    project.hide('hidable_file.py')

    hidden_files = project.list_hidden()

    assert len(project.list_hidden()) == 2
    shutil.rmtree(tmp_project_path)

def test_is_hidden(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    project.hide('hidable_directory')
    project.hide('hidable_directory/hidable_directory_file.py')
    project.hide('hidable_file.py')

    print(project.list_hidden(directories=True))

    assert project.is_hidden('hidable_directory')

    assert len(project.list_hidden()) == 2
    assert len(project.list_hidden(directories=True)) == 3
    shutil.rmtree(tmp_project_path)



def test_unhide(tmp_path):

    shutil.copytree(project_path, tmp_path / 'hidable-project')
    tmp_project_path = tmp_path / 'hidable-project'
    project = Project(tmp_project_path)

    project.hide('hidable_directory')
    project.hide('hidable_directory/hidable_directory_file.py')
    project.hide('hidable_file.py')
    project.unhide('hidable_directory')
    project.unhide('hidable_directory/hidable_directory_file.py')
    project.unhide('hidable_file.py')

    assert not project.is_hidden('hidable_directory')

    assert len(project.list_hidden()) == 0
    assert len(project.list_hidden(directories=True)) == 0

    shutil.rmtree(tmp_project_path)


#def test_inline_hide(tmp_path):
#        
#    rendered = Project(project_path).render(output=(tmp_path / 'hidable-project-rendered'))
#
#
#    assert utils.are_dir_trees_equal(rendered.root, expected_path), \
#                f"Trees are not equal {rendered.root} {expected_path}"
#
#    shutil.rmtree(tmp_path)

