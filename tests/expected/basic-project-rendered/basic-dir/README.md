This is a basic README

<!--| p(values.project.name) |-->
BasicProject
<!--|end|-->

<!--| p(values.project.description) |-->
A basic project
<!--|end|-->

<!--| jinja('Rendered Using Jinja: values.project.name: {{ values.project.name }}') |-->
Rendered Using Jinja: values.project.name: BasicProject
<!--|end|-->
    
<!--| jinja("Rendered Using Jinja Macro: hello(values.project.name): {{ hello(values.project.name) }}") |-->
Rendered Using Jinja Macro: hello(values.project.name): Hello BasicProject!
<!--|end|-->


<!--| jinja("{% include 'demo-template.txt' %}") |-->
This is a demo template: Hello world! BasicProject
<!--|end|-->