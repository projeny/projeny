
from app.database import Column, Model, SurrogatePK, db, reference_col, relationship

# [[[cog flask_sqlalchemy(values.spark.openapi.components) ]]]


class ApiResponse(SurrogatePK, Model):
    """The ApiResponse Model"""

    __tablename__ = "api_response"
    code = Column(db.Integer, nullable=True, unique=False, default=None, comment='code:integer')
    message = Column(db.String, nullable=True, unique=False, default=None, comment='message:string')

    def __init__(self,  code, message,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, code=code, message=message, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<ApiResponse(code={self.code},message={self.message},)>"

class MetricDetail(SurrogatePK, Model):
    """The MetricDetail Model"""

    __tablename__ = "metric_detail"
    column = Column(db.String, nullable=True, unique=False, default=None, comment='column:object')

    def __init__(self,  column,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, column=column, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<MetricDetail(column={self.column},)>"

class DimensionDetail(SurrogatePK, Model):
    """The DimensionDetail Model"""

    __tablename__ = "dimension_detail"
    column = Column(db.String, nullable=True, unique=False, default=None, comment='column:object')

    def __init__(self,  column,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, column=column, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<DimensionDetail(column={self.column},)>"

class ColumnDetail(SurrogatePK, Model):
    """The ColumnDetail Model"""

    __tablename__ = "column_detail"
    table = Column(db.String, nullable=True, unique=False, default=None, comment='table:string')
    column = Column(db.String, nullable=True, unique=False, default=None, comment='column:string')
    dataType = Column(db.String, nullable=True, unique=False, default=None, comment='dataType:string')
    nullable = Column(db.Boolean, nullable=True, unique=False, default=None, comment='nullable:boolean')
    nullValue = Column(db.String, nullable=True, unique=False, default=None, comment='nullValue:string')

    def __init__(self,  table, column, dataType, nullable, nullValue,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, table=table, column=column, dataType=dataType, nullable=nullable, nullValue=nullValue, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<ColumnDetail(table={self.table},column={self.column},dataType={self.dataType},nullable={self.nullable},nullValue={self.nullValue},)>"

class JoinDetail(SurrogatePK, Model):
    """The JoinDetail Model"""

    __tablename__ = "join_detail"
    leftTable = Column(db.String, nullable=True, unique=False, default=None, comment='leftTable:string')
    leftColumn = Column(db.String, nullable=True, unique=False, default=None, comment='leftColumn:string')
    rightTable = Column(db.String, nullable=True, unique=False, default=None, comment='rightTable:string')
    joinType = Column(db.String, nullable=True, unique=False, default=None, comment='joinType:string')

    def __init__(self,  leftTable, leftColumn, rightTable, joinType,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, leftTable=leftTable, leftColumn=leftColumn, rightTable=rightTable, joinType=joinType, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<JoinDetail(leftTable={self.leftTable},leftColumn={self.leftColumn},rightTable={self.rightTable},joinType={self.joinType},)>"

class ClusterDetail(SurrogatePK, Model):
    """The ClusterDetail Model"""

    __tablename__ = "cluster_detail"
    status = Column(db.String, nullable=True, unique=False, default=None, comment='status:string')
    lastAccesedOn = Column(db.DateTime, nullable=True, unique=False, default=None, comment='lastAccesedOn:string')
    lastStartedOn = Column(db.DateTime, nullable=True, unique=False, default=None, comment='lastStartedOn:string')
    createdOn = Column(db.DateTime, nullable=True, unique=False, default=None, comment='createdOn:string')
    jdbc = Column(db.String, nullable=True, unique=False, default=None, comment='jdbc:string')
    size = Column(db.Float, nullable=False, unique=False, default=3, comment='size:number')
    id = Column(db.String, nullable=False, unique=True, default=None, comment='Unique ID of cluster')

    def __init__(self,  status, lastAccesedOn, lastStartedOn, createdOn, jdbc, size, id,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, status=status, lastAccesedOn=lastAccesedOn, lastStartedOn=lastStartedOn, createdOn=createdOn, jdbc=jdbc, size=size, id=id, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<ClusterDetail(status={self.status},lastAccesedOn={self.lastAccesedOn},lastStartedOn={self.lastStartedOn},createdOn={self.createdOn},jdbc={self.jdbc},size={self.size},id={self.id},)>"

class QubeDeploymentDetail(SurrogatePK, Model):
    """The QubeDeploymentDetail Model"""

    __tablename__ = "qube_deployment_detail"
    status = Column(db.String, nullable=True, unique=False, default=None, comment='status:string')
    lastAccesedOn = Column(db.DateTime, nullable=True, unique=False, default=None, comment='lastAccesedOn:string')
    deployedOn = Column(db.DateTime, nullable=True, unique=False, default=None, comment='deployedOn:string')
    jdbc = Column(db.String, nullable=True, unique=False, default=None, comment='jdbc:string')
    size = Column(db.Float, nullable=True, unique=False, default=None, comment='size:number')
    id = Column(db.String, nullable=True, unique=False, default=None, comment='id:string')

    def __init__(self,  status, lastAccesedOn, deployedOn, jdbc, size, id,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, status=status, lastAccesedOn=lastAccesedOn, deployedOn=deployedOn, jdbc=jdbc, size=size, id=id, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<QubeDeploymentDetail(status={self.status},lastAccesedOn={self.lastAccesedOn},deployedOn={self.deployedOn},jdbc={self.jdbc},size={self.size},id={self.id},)>"

class TableDetail(SurrogatePK, Model):
    """The TableDetail Model"""

    __tablename__ = "table_detail"
    name = Column(db.String, nullable=True, unique=False, default=None, comment='Name of Table')
    columns = Column(db.String, nullable=True, unique=False, default=None, comment='columns:array')
    source = Column(db.String, nullable=True, unique=False, default=None, comment='the SparkSQL "path" to data source')
    partitionedBy = Column(db.String, nullable=True, unique=False, default=None, comment='partitionedBy:array')

    def __init__(self,  name, columns, source, partitionedBy,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, name=name, columns=columns, source=source, partitionedBy=partitionedBy, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<TableDetail(name={self.name},columns={self.columns},source={self.source},partitionedBy={self.partitionedBy},)>"

class QubeDetail(SurrogatePK, Model):
    """The QubeDetail Model"""

    __tablename__ = "qube_detail"
    factTable = Column(db.String, nullable=True, unique=False, default=None, comment='The Fact Table')
    joins = Column(db.String, nullable=True, unique=False, default=None, comment='joins:array')
    metrics = Column(db.String, nullable=True, unique=False, default=None, comment='metrics:array')
    dimensions = Column(db.String, nullable=True, unique=False, default=None, comment='dimensions:array')

    def __init__(self,  factTable, joins, metrics, dimensions,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, factTable=factTable, joins=joins, metrics=metrics, dimensions=dimensions, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<QubeDetail(factTable={self.factTable},joins={self.joins},metrics={self.metrics},dimensions={self.dimensions},)>"

class QueryResult(SurrogatePK, Model):
    """The QueryResult Model"""

    __tablename__ = "query_result"
    result = Column(db.String, nullable=True, unique=False, default=None, comment='the result of the data')

    def __init__(self,  result,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, result=result, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<QueryResult(result={self.result},)>"

class QueryDetail(SurrogatePK, Model):
    """The QueryDetail Model"""

    __tablename__ = "query_detail"
    sql = Column(db.String, nullable=True, unique=False, default=None, comment='the sql query string')
    uuid = Column(db.String, nullable=True, unique=False, default=None, comment='unique identifier of executed query')
    milliseconds = Column(db.Integer, nullable=True, unique=False, default=None, comment='duration of query')

    def __init__(self,  sql, uuid, milliseconds,  **kwargs):
        """Create instance."""
        db.Model.__init__(self, sql=sql, uuid=uuid, milliseconds=milliseconds, **kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return f"<QueryDetail(sql={self.sql},uuid={self.uuid},milliseconds={self.milliseconds},)>"

# [[[end]]]