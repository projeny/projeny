
def snake_case(txt):
    """Converts to snake case"""
    import re
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', txt)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def function_name(name):
    """Converts to pythonic function name"""
    return snake_case(name)

def function_call_kwargs(properties):
    if isinstance(properties,list):
        # properties is actually list of parameters
        return ','.join([f"{p['name']}={p['name']}" for p in properties])
    elif isinstance(properties, dict):
        return ','.join([f" {name}={name}" for name,prop in properties.items()]) + ', '
    else:
        raise NotImplementedError

def parameters_in(properties,query_or_path):
    if isinstance(properties,list):
        # properties is actually list of parameters
        return [p for p in properties if p['in']==query_or_path]
    else:
        raise NotImplementedError

def function_call_args(properties):
    if isinstance(properties,list):
        # properties is actually list of parameters
        return ','.join([p['name'] for p in properties])
    elif isinstance(properties, dict):
        return  ','.join([f" {name}" for name,prop in properties.items()]) + ', '
    else:
        raise NotImplementedError

# Takens from https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#data-types
# and https://www.oreilly.com/library/view/essential-sqlalchemy/9780596516147/ch04.html

oa3_to_sqla = {
    'integer' : {
        'format': {
            '': 'Integer',
            'int32': 'Integer',
            'int64': 'Numeric'
        }
    },
    'number' : {
        'format': {
            '':'Float',
            'float' : 'Float',
            'double' : 'Float'
        }
    },
    'string' : {
        'format': {
            '' : 'String',
            'byte' : 'Binary',
            'binary' : 'Binary',
            'date' : 'DateTime',
            'date-time' : 'DateTime',
            'password' : 'String'
        }
    },
    'boolean' : 'Boolean',
}

def to_sql_type(property):
    oa3_type = property['type'].lower()
    if oa3_type not in oa3_to_sqla:
        return 'String'

    sqla_type = oa3_to_sqla[oa3_type]

    if 'format' not in sqla_type:
        return sqla_type

    if 'format' not in property or property['format'] not in sqla_type['format']:
        return sqla_type['format']['']

    return sqla_type['format'][property['format']]

def sqlalchemy_field(property,name,required=False):
    default = property.get('default', None)
    nullable = not required and not default
    unique = property.get('uniqueItems',False)
    comment = property.get('description',f"{name}:{property['type']}")
    return f"Column(db.{ to_sql_type(property) }, nullable={nullable}, unique={unique}, default={default}, comment='{comment}')"

def function_doc(docstring):
    return f'"""{docstring}"""'